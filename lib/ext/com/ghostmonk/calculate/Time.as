package com.ghostmonk.calculate
{
	public class Time
	{
		public static function minutes(seconds:Number):Number
		{
			return seconds/60;
		}
		
		public static function seconds(milliseconds:Number):Number
		{
			return milliseconds*100/60;
		}

	}
}