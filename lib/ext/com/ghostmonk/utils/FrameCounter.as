package com.ghostmonk.utils
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.Font;
	import flash.text.TextField;
	import flash.text.TextFormat;

	public class FrameCounter extends Sprite
	{
		private var _fpsOutput:TextField = new TextField();
		private var _secTracker:int = new Date().getSeconds();
		private var _frameTracker:int = 0;
		private var _color:Number = 0x444444;
		private var _font:Font;
		private var _textFormat:TextFormat;
		
		public function FrameCounter(font:Font)
		{
			_font = font;
			_fpsOutput.embedFonts = true;
			_textFormat = new TextFormat(_font.fontName, 8, 0x444444);
			_fpsOutput.defaultTextFormat = _textFormat;
			_fpsOutput.height = 10;
			
			addChild(_fpsOutput);
			_fpsOutput.textColor = _color;	
		}
		
		public function start():void
		{
			addEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		
		public function stop():void
		{
			removeEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		
		private function onEnterFrame(e:Event):void
		{
			var now:Date = new Date();
			var sec:int = now.getSeconds();
			
			if(_secTracker == sec)
			{
				_frameTracker++;
			}
			else
			{
				_fpsOutput.text = "FPS: " + _frameTracker;
				_frameTracker = 0;	
			}
			
			_secTracker = sec;
		}
		
	}
}