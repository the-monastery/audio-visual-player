package com.ghostmonk.organic.data
{
	public class PerlinValues
	{
		
		private var _baseX:Number = 50;
		private var _baseY:Number = 50;
		private var _octaves:uint = 3;
		private var _randomSeed:int = 10;
		private var _stitch:Boolean = false;
		private var _fractalNoise:Boolean = false;
		private var _greyScale:Boolean = false;
		private var _channelOptions:uint = 6;
		private var _isBitmapVisible:Boolean = false;
		private var _speed:Number = 1;
		
		public function setBaseX		(value:Number)	:void	{	_baseX = value;			}
		public function setBaseY		(value:Number)	:void	{	_baseY = value;			}
		public function setRandomSeed	(value:int)		:void	{	_randomSeed = value;	}
		public function setStitch		(value:Boolean)	:void	{	_stitch = value;		}
		public function setFractalNoise	(value:Boolean)	:void	{	_fractalNoise = value;	}
		public function setGreyScale	(value:Boolean)	:void	{	_greyScale = value;		}
		
		public function getBaseX()			:Number		{	return _baseX;			}
		public function getBaseY()			:Number		{	return _baseY;			}
		public function getRandomSeed()		:int		{	return _randomSeed;		}
		public function getStitch()			:Boolean	{	return _stitch;			}
		public function getFractalNoise()	:Boolean	{	return _fractalNoise;	}
		public function getGreyScale()		:Boolean	{	return _greyScale;		}
		public function getSpeed()			:Number		{  	return _speed;			}
		public function getOctaves()		:uint		{	return _octaves;		}
		
		public function PerlinValues()
		{
		}

	}
}