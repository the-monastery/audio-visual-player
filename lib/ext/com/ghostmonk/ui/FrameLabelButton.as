package com.ghostmonk.ui
{
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import flash.display.MovieClip;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	
	[Event(type="flash.events.MouseEvent", name="rollOver")]
	[Event(type="flash.events.MouseEvent", name="rollOut")]
	[Event(type="flash.events.MouseEvent", name="mouseDown")]
	[Event(type="flash.events.MouseEvent", name="mouseUp")]
	
	public class FrameLabelButton extends EventDispatcher
	{
		private var _clip:MovieClip;
		private var _mouseOver:int;
		private var _mouseDown:int;
		
		public function get view():MovieClip
		{
			return _clip;
		}
		
		public function FrameLabelButton(clip:MovieClip)
		{
			_clip = clip;
			setFrameDestinations();
			enable();
			_clip.mouseChildren = false;
		}
		
		public function enable():void
		{
			_clip.buttonMode = true;
			_clip.mouseEnabled = true;
			_clip.addEventListener(MouseEvent.ROLL_OVER, onRollover);
			_clip.addEventListener(MouseEvent.ROLL_OUT, onRollout);
			_clip.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			_clip.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		}
		
		public function disable():void
		{
			_clip.buttonMode = false;
			_clip.mouseEnabled = false;
			_clip.removeEventListener(MouseEvent.ROLL_OVER, onRollover);
			_clip.removeEventListener(MouseEvent.ROLL_OUT, onRollout);
			_clip.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			_clip.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		}
		
		private function setFrameDestinations():void
		{
			_clip.gotoAndStop("mouseOver");
			_mouseOver = _clip.currentFrame;
			_clip.gotoAndStop("mouseDown");
			_mouseDown = _clip.currentFrame;
			_clip.gotoAndStop("mouseOut");
		}
		
		private function onRollover(e:MouseEvent):void  
		{ 	
			tweenTo(_mouseOver);
			dispatchEvent(e);	
		}
		
		private function onRollout(e:MouseEvent):void 	
		{ 	
			tweenTo(1);
			dispatchEvent(e); 			
		}
		
		private function onMouseDown(e:MouseEvent):void 
		{ 	
			tweenTo(_mouseDown);
			dispatchEvent(e);	
		}
		
		private function onMouseUp(e:MouseEvent):void   
		{ 	
			tweenTo(_mouseOver);
			dispatchEvent(e);	
		}
		
		//TODO Find a way to get a reference to the stage to discern the framerate... 
		//very simple solution right now is using 31, which is the most common framerate I use
		private function tweenTo(destFrame:int):void
		{
			Tweener.removeTweens(_clip);
			//Find the difference between current frame and destination frame, make it positive and divide by frameRate
			var transTime:Number = Math.abs((destFrame - _clip.currentFrame))/31;//************stage.framerate
			Tweener.addTween(_clip, {_frame:destFrame, time:transTime, transition:Equations.easeNone});
		}
		
	}
}