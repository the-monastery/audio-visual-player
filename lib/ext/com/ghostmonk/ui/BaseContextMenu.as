package com.ghostmonk.ui
{
	import flash.display.Sprite;
	import flash.events.ContextMenuEvent;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.ui.ContextMenu;
	import flash.ui.ContextMenuItem;
	
	public class BaseContextMenu
	{
		private var _contextMenu:ContextMenu;
		private var _links:Object;
		private var _app:Sprite;
		
		public function BaseContextMenu(app:Sprite)
		{
			_links = [];
			_contextMenu = new ContextMenu();
			_contextMenu.hideBuiltInItems();
			_app = app;
		}
		
		public function addLink(label:String, url:String):void
		{
			var link:ContextMenuItem = new ContextMenuItem(label);
			link.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, onSelect);
			_contextMenu.customItems.push(link);
			_links[label] = url;
			_app.contextMenu = _contextMenu;
		}
		
		private function onSelect(e:ContextMenuEvent):void
		{
			navigateToURL(new URLRequest(_links[ContextMenuItem(e.target).caption]), "_blank");
		}
	}
}