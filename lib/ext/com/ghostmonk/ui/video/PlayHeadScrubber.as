package com.ghostmonk.ui.video
{
	import flash.display.Sprite;
	
	public class PlayHeadScrubber extends Sprite
	{
		private var _scrollDrag:HorizontalScrollDrag;
		
		public function PlayHeadScrubber(progressBar:Sprite, scrubBar:Sprite)
		{
			_scrollDrag = new HorizontalScrollDrag(progressBar, scrubBar);
			addChild(_scrollDrag);
		}

	}
}