/**
 *	Written by:Nicholas Hillier Nov. 18 2008 
 *  This contains all the base functionality of buttons with the expected over, out and click states
*/
package com.ghostmonk.ui
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;

	public class AbstractButton extends Sprite
	{
		public function AbstractButton()
		{
			
		}
		
		private function onRollOver(e:MouseEvent):void
		{
			
		}
		
		private function onRollOut(e:MouseEvent):void
		{
			
		}
		
		private function onMouseClick(e:MouseEvent):void
		{
			
		}
	}
}