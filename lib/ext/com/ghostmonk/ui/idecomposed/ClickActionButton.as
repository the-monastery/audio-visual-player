package com.ghostmonk.ui.idecomposed
{
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.ghostmonk.ui.FrameLabelButton;
	
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	
	public class ClickActionButton
	{
		private var _clip:FrameLabelButton;
		
		private var _callBack:Function;
		
		public function get view():MovieClip
		{
			return _clip.view;
		}
		
		public function ClickActionButton(clip:MovieClip, callBack:Function)
		{
			_clip = new FrameLabelButton(clip);
			_callBack = callBack;
			enable();
		}
		
		public function enable():void
		{
			_clip.enable();
			_clip.view.addEventListener(MouseEvent.CLICK, onMouseClick);
		}
		
		public function disable():void
		{
			_clip.disable();
			_clip.view.removeEventListener(MouseEvent.CLICK, onMouseClick);
		}
		
		private function onMouseClick(e:MouseEvent):void
		{
			_callBack(e);
		}
		
	}
}