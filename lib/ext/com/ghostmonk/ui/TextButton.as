package com.ghostmonk.ui
{
	import com.ghostmonk.text.EmbeddedText;
	
	import flash.display.*;
	import flash.events.MouseEvent;

	public class TextButton extends Sprite
	{
		private var _textColor:uint;
		private var _baseColor:uint;
		
		private var _text:EmbeddedText;
		private var _background:Bitmap;
		
		private var _width:Number;
		
		override public function get width():Number
		{
			return _width;
		}
		
		public function TextButton(font:String, textSize:Number, label:String, textColor:uint, baseColor:uint, width:Number)
		{
			_text = new EmbeddedText(font, textSize, textColor);
			_text.width = width;
			_text.text = label;
			
			_textColor = textColor;
			_baseColor = baseColor;
			_width = width;
			
			_background = new Bitmap(new BitmapData(width, _text.height, true, 0));
			addChild(_background);
			addChild(_text); 
			
			buttonMode = true;
			mouseChildren = false;
			
			addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
			addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
		}
		
		private function onMouseOver(e:MouseEvent):void
		{
			if(_baseColor)
			{
				_text.color = _baseColor;
				_background.bitmapData = new BitmapData(width, _text.height, false, _textColor);
			}	
		}
		
		protected function onMouseOut(e:MouseEvent):void
		{
			_text.color = _textColor;
			_background.bitmapData.dispose();
			_background.bitmapData = new BitmapData(width, _text.height, true, 0);
		}
		
	}
}