package com.ghostmonk.media.audio
{
	import com.ghostmonk.events.audio.AudioEvent;
	import com.ghostmonk.events.video.NetStreamEvent;
	
	import flash.events.*;
	import flash.media.*;
	import flash.net.URLRequest;
	
	[Event (name="stop", type="com.ghostmonk.events.video.NetStreamEvent")]
	
	public class MusicPlayer extends EventDispatcher
	{
		private var _soundPlayer:Sound;
		private var _id3:ID3Info;
		private var _channel:SoundChannel;
		private var _pausePosition:int;
		private var _context:SoundLoaderContext;
		
		private var _isReady:Boolean;
		private var _isPlaying:Boolean;
		
		public function get percentLoaded():Number
		{
			return _soundPlayer.bytesTotal ? _soundPlayer.bytesLoaded/_soundPlayer.bytesTotal : 0;
		}
		
		public function get duration():Number
		{
			var dur:Number = (_soundPlayer.length/(_soundPlayer.bytesLoaded/_soundPlayer.bytesTotal))/1000;
			if(dur < 0) dur = 0;
			return dur;
		}
		
		public function get length():Number
		{
			return _soundPlayer.length/1000;
		}
		
		public function get time():Number
		{	
			return _channel.position/1000;
		}
		
		public function MusicPlayer()
		{
			_isReady = false;
			_isPlaying = false;
			_context = new SoundLoaderContext(1,true);
			createNewSound();
		}
		
		public function pause():void
		{
			_pausePosition = _channel.position;
			_channel.stop();
			_isPlaying = false;
		}
		
		public function play():void
		{
			if(!_isPlaying)
			{
				_channel = _soundPlayer.play(_pausePosition);
			}
			_isPlaying = true;
		}
		
		public function seek(value:Number):void
		{
			_channel.stop();
			_channel = _soundPlayer.play(value*1000); 
		}
		
		public function load(url:String):void
		{
			SoundMixer.stopAll();
			_channel.stop();
			_isPlaying = false;
			createNewSound();
			_soundPlayer.load(new URLRequest(url));
			play();
			_channel.addEventListener(Event.ACTIVATE, onChannelActivate);
			_channel.addEventListener(Event.DEACTIVATE, onChannelDeactivate);
			_channel.addEventListener(Event.SOUND_COMPLETE, onChannelComplete);
		}
		
		public function close():void
		{
			_channel.stop();
		}
		
		private function createNewSound():void
		{
			try
			{
				_soundPlayer.close()
			}
			catch(e:Error)
			{
				trace(e.name);
			}
			
			_pausePosition = 0;
			_soundPlayer = null;
			_channel = null;
			
			_soundPlayer = new Sound();
			_channel = new SoundChannel();
			
			_soundPlayer.addEventListener(Event.ACTIVATE, onSoundActivate);
			_soundPlayer.addEventListener(Event.DEACTIVATE, onSoundDeactivate);
			_soundPlayer.addEventListener(Event.COMPLETE, onComplete);
			_soundPlayer.addEventListener(Event.ID3, onID3);
			_soundPlayer.addEventListener(Event.OPEN, onOpen);
			_soundPlayer.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
			_soundPlayer.addEventListener(ProgressEvent.PROGRESS, onProgress);
		}
		
		private function onChannelActivate(e:Event):void
		{
			trace("channel activate");
		}
		
		private function onChannelDeactivate(e:Event):void
		{
			trace("channel deactivagte");
		}
		
		private function onChannelComplete(e:Event):void
		{
			trace("channel complete");
			dispatchEvent(new NetStreamEvent(NetStreamEvent.STOP));
		}
		
		private function onSoundActivate(e:Event):void
		{
			trace("sound activate");
		}
		
		private function onSoundDeactivate(e:Event):void
		{
			trace("sound deactivate");
		}
		
		private function onComplete(e:Event):void
		{
			trace("sound complete");
		}
		
		private function onID3(e:Event):void
		{
			trace("on ID3");
			_id3 = _soundPlayer.id3;
			trace("ALBUM: "+_id3.album);
			trace("ARTIST: "+_id3.artist);
			trace("COMMENT: "+_id3.comment);
			trace("GENRE: "+_id3.genre);
			trace("SONG NAME: "+_id3.songName);
			trace("TRACK: "+_id3.track);
			trace("YEAR: "+_id3.year);
			dispatchEvent(new AudioEvent(AudioEvent.ON_ID3, _id3));
		}
		
		private function onOpen(e:Event):void
		{
			trace("on open");	
		}
		
		private function onIOError(e:IOErrorEvent):void
		{
			trace("on IOError");
		}
		
		private function onProgress(e:ProgressEvent):void
		{
			//trace("onProgress");
		}

	}
}