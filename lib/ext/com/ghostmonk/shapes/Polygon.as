package
{
	import flash.display.Sprite;

	public class Polygon
	{
		
		public function Polygon(){}
		
		public static function drawShape(sides:int, color:Number, alpha:Number, width:Number):Sprite
		{
			var polygon:Sprite = new Sprite();
			var radius:Number = width/2;
			var rad:Number = (360/sides) * Math.PI/180;
			
			polygon.graphics.beginFill(color,alpha);
					 
			for(var i:int = 1; i <= sides; i++)
			{
				i == 1 
				? polygon.graphics.moveTo(Math.cos(rad*i)*radius,Math.sin(rad*i)*radius)
				: polygon.graphics.lineTo(Math.cos(rad*i)*radius,Math.sin(rad*i)*radius);	
			}
			
			polygon.graphics.endFill();
			return polygon;
		} 
		
	}
}