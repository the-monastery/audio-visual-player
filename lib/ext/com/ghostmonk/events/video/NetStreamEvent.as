package com.ghostmonk.events.video
{
	import flash.events.Event;

	public class NetStreamEvent extends Event
	{
		public static const FLUSH:String = "NetStream.Buffer.Flush";
		public static const FULL:String = "NetStream.Buffer.Full";
		public static const EMPTY:String = "NetStream.Buffer.Empty";
		public static const START:String = "NetStream.Play.Start";
		public static const STOP:String = "NetStream.Play.Stop";
		
		public function NetStreamEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
	}
}