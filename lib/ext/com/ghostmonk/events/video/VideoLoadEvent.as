package com.ghostmonk.events.video
{
	import flash.events.Event;

	public class VideoLoadEvent extends Event
	{
		public static const LOAD_VIDEO:String = "loadVideo";
		
		private var _url:String;
		
		public function get url():String
		{
			return _url;
		}
		
		public function VideoLoadEvent(type:String, url:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			_url = url;
		}
		
		override public function clone():Event
		{
			return new VideoLoadEvent(type, url);
		}
		
	}
}