package com.ghostmonk.events.audio
{
	import flash.events.Event;
	import flash.media.ID3Info;

	public class AudioEvent extends Event
	{
		public static const ON_ID3:String = "onId3";
		
		private var _id3:ID3Info;
		
		public function get id3():ID3Info
		{
			return _id3;
		}
		
		public function AudioEvent(type:String, id3:ID3Info, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			_id3 = id3;
		}
		
		override public function clone():Event
		{
			return new AudioEvent(type, id3);
		}
		
	}
}