package com.sqeakecleanblog.mediaplayer.events
{
	import flash.events.Event;

	public class PlaylistNavEvent extends Event
	{
		public static const NEXT_PREVIOUS:String = "next_previous"; 
		public static const PLAYLIST_CHANGE:String = "playlistChange";
		
		private var _value:int;
		
		public function get value():int
		{
			return _value;
		}
		
		public function PlaylistNavEvent(type:String, value:int, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			_value = value;
		}
		
		override public function clone():Event
		{
			return new PlaylistNavEvent(type, value);
		}
		
	}
}