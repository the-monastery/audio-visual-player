package com.sqeakecleanblog.mediaplayer.events
{
	import flash.events.Event;

	public class SwitchEvent extends Event
	{
		public static const FLIP:String = "flip";
		
		private var _index:int;
		
		public function get index():int { return _index; }
		
		public function SwitchEvent(type:String, index:int, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			_index = index;
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new SwitchEvent(type, index);
		}
		
	}
}