package com.sqeakecleanblog.mediaplayer.events
{
	import flash.events.Event;

	public class ShuttleEvent extends Event
	{
		public static const SHUTTLE:String = "shuttle";
		
		private var _value:Number;
		
		public function get value():Number
		{
			return _value;
		}
		
		public function ShuttleEvent(type:String, value:Number, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			_value = value;
		}
		
		override public function clone():Event
		{
			return new ShuttleEvent(type, value);
		}
		
	}
}