package com.sqeakecleanblog.mediaplayer.controller
{
	import com.ghostmonk.media.SoundSpectrum;
	import com.sqeakecleanblog.mediaplayer.model.PlaylistDataProxy;
	import com.sqeakecleanblog.mediaplayer.model.structures.StartupData;
	import com.sqeakecleanblog.mediaplayer.view.*;
	
	import org.puremvc.as3.multicore.interfaces.INotification;
	import org.puremvc.as3.multicore.patterns.command.SimpleCommand;

	public class StartupCommand extends SimpleCommand
	{
		override public function execute(notification:INotification):void
		{
			var startupData:StartupData = notification.getBody() as StartupData;
			
			facade.registerProxy(new PlaylistDataProxy(startupData.playListURL, startupData.startNode, startupData.itemID));
			
			facade.registerMediator(new SoundSpectrumMediator(new SoundSpectrum()));
			facade.registerMediator(new MediaPlayerMediator());
			facade.registerMediator(new MediaControlsMediator());
			facade.registerMediator(new StageMediator(startupData.stage));
		}
	}
}