package com.sqeakecleanblog.mediaplayer.model.structures
{
	public class PlayListItem
	{
		private var _id:String;
		private var _url:String;
		private var _title:String;
		private var _imgURL:String;
		
		public function get id():String
		{
			return _id;
		}
		
		public function get url():String
		{
			return _url;
		}
		
		public function get title():String
		{
			return _title;
		}
		
		public function get imgURL():String
		{
			return _imgURL;
		}
		
		public function PlayListItem(xml:XML)
		{
			_id = xml.@id.toString();
			_url = xml.url.toString();
			_title = xml.title.toString();
			_imgURL = xml.img.@src.toString();
		}
		
		public function toString():String
		{
			return 	"ID: " + id + "\n"+
					"URL: " + url + "\n"+
					"TITLE: " + title + "\n"+
					"IMG: " + imgURL + "\n";
		}

	}
}