package com.sqeakecleanblog.mediaplayer.model.structures
{
	import com.ghostmonk.ui.TimeDisplay;
	import com.ghostmonk.ui.idecomposed.ClickActionButton;
	import com.ghostmonk.ui.idecomposed.Scroller;
	import com.sqeakecleanblog.mediaplayer.components.ui.InteractivePlaylist;
	
	import flash.geom.Point;
	
	public class PanelAssets
	{
		private var _playBtn:ClickActionButton;
		private var _pauseBtn:ClickActionButton;
		private var _backBtn:ClickActionButton;
		private var _nextBtn:ClickActionButton;
		private var _fullScreen:ClickActionButton;
		private var _time:TimeDisplay;
		private var _playhead:Scroller;
		private var _playlist:InteractivePlaylist;
		
		private var _playPoint:Point;
		private var _pausePoint:Point;
		private var _backPoint:Point;
		private var _nextPoint:Point;
		private var _fullPoint:Point;
		private var _timePoint:Point;
		private var _scrollPoint:Point;
		private var _playlistPoint:Point;
		
		public function get playBtn():ClickActionButton		{return _playBtn;}
		public function get pauseBtn():ClickActionButton	{return _pauseBtn;}
		public function get backBtn():ClickActionButton		{return _backBtn;}
		public function get nextBtn():ClickActionButton		{return _nextBtn;}
		public function get fullScreen():ClickActionButton	{return _fullScreen;}
		public function get time():TimeDisplay				{return _time;}
		public function get playhead():Scroller				{return _playhead;}
		public function get playlist():InteractivePlaylist	{return _playlist;}
		
		public function PanelAssets(play:ClickActionButton, pause:ClickActionButton, back:ClickActionButton, next:ClickActionButton, full:ClickActionButton, time:TimeDisplay, playHead:Scroller, playlist:InteractivePlaylist)
		{
			_playBtn = play;
			_pauseBtn = pause;
			_backBtn = back;
			_nextBtn = next;
			_fullScreen = full;
			_time = time;
			_playhead = playHead;
			_playlist = playlist;
			
			_playPoint = new Point(play.view.x, play.view.y);
			_pausePoint = new Point(pause.view.x, pause.view.y);
			_backPoint = new Point(back.view.x, back.view.y);
			_nextPoint = new Point(next.view.x, next.view.y);
			_fullPoint = new Point(full.view.x, full.view.y);
			_timePoint = new Point(time.view.x, time.view.y);
			_scrollPoint = new Point(playHead.view.x, playHead.view.y);
			_playlistPoint = new Point(playlist.view.x, playlist.view.y);
		}
		
		public function resetPositions():void
		{
			_playBtn.view.x = _playPoint.x;
			_playBtn.view.y = _playPoint.y;
			
			_pauseBtn.view.x = _pausePoint.x;
			_pauseBtn.view.y = _pausePoint.y;
			
			_nextBtn.view.x = _nextPoint.x;
			_nextBtn.view.y = _nextPoint.y;
			
			_backBtn.view.x = _backPoint.x;
			_backBtn.view.y = _backPoint.y;
			
			_fullScreen.view.x = _fullPoint.x;
			_fullScreen.view.y = _fullPoint.y;
			
			_time.view.x = _timePoint.x;
			_time.view.y = _timePoint.y;
			
			_playhead.view.x = _scrollPoint.x;
			_playhead.view.y = _scrollPoint.y;
			
			_playlist.view.x = _playlistPoint.x;
			_playlist.view.y = _playlistPoint.y;
		}
		
	}
}