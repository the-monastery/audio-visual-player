package com.sqeakecleanblog.mediaplayer.model.structures
{
	import flash.display.Stage;
	
	public class StartupData
	{
		private var _playlistURL:String;
		private var _itemID:String;
		private var _startNode:int;
		private var _stage:Stage;
		
		public function get stage():Stage
		{
			return _stage;
		}
		
		public function get playListURL():String
		{
			return _playlistURL;
		}
		
		public function get startNode():int
		{
			return _startNode;
		}
		
		public function get itemID():String
		{
			return _itemID;
		}
		
		public function StartupData(stage:Stage, playlistURL:String, startNode:int, itemID:String = "")
		{
			_stage = stage;
			_playlistURL = playlistURL;
			_startNode = startNode;
			_itemID = itemID;
		}

	}
}