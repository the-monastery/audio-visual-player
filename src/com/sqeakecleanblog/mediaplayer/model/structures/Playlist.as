package com.sqeakecleanblog.mediaplayer.model.structures
{
	public class Playlist
	{
		private var _playList:Array;
		private var _current:int;
		
		public function getItem(node:int):PlayListItem
		{
			_current = node;
			return _playList[node] as PlayListItem;
		}
		
		private function getIndexByName(id:String):int
		{
			var target:int = 0;
			for(var i:int = 0; i<_playList.length; i++)
			{
				if(PlayListItem(_playList[i]).id == id)
				{ 
					target = i;
				}
			}
			return target;
		}
		
		public function get currentNode():int
		{
			return _current;
		}
		
		public function get length():int
		{
			return _playList.length;
		}
		
		public function get currentItem():PlayListItem
		{
			return _playList[_current];
		}
		
		public function Playlist(xml:XML, startNode:int, startID:String)
		{
			_playList = new Array();
			
			for each(var item:XML in xml.item)
			{
				_playList.push(new PlayListItem(item));
			}
			
			_current = startNode == -1 ? getIndexByName(startID) : startNode; 
		}

	}
}