package com.sqeakecleanblog.mediaplayer.model
{
	import com.ghostmonk.net.XMLLoader;
	import com.sqeakecleanblog.mediaplayer.ApplicationFacade;
	import com.sqeakecleanblog.mediaplayer.model.structures.Playlist;
	
	import org.puremvc.as3.multicore.patterns.proxy.Proxy;

	public class PlaylistDataProxy extends Proxy
	{
		public static const NAME:String = "PlaylistDataProxy";
		private var _startNode:int;
		private var _startID:String;
		
		public function get playlist():Playlist
		{
			return data as Playlist;
		}
		
		public function PlaylistDataProxy(url:String, startNode:int, startID:String)
		{
			super(NAME);
			_startNode = startNode;
			_startID = startID;
			var xmlLoader:XMLLoader = new XMLLoader(url, onXMLComplete);
		}
		
		private function onXMLComplete(xml:XML):void
		{
			data = new Playlist(xml, _startNode, _startID);
			sendNotification(ApplicationFacade.PLAYLIST_LOADED, data);
		}
		
	}
}