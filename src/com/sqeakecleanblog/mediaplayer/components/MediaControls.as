package com.sqeakecleanblog.mediaplayer.components
{
	import assets.VideoControlPanel;
	
	import com.ghostmonk.events.PercentageEvent;
	import com.ghostmonk.events.video.VideoControlEvent;
	import com.ghostmonk.ui.*;
	import com.ghostmonk.ui.idecomposed.*;
	import com.sqeakecleanblog.mediaplayer.components.ui.InteractivePlaylist;
	import com.sqeakecleanblog.mediaplayer.events.*;
	import com.sqeakecleanblog.mediaplayer.model.structures.PanelAssets;
	import com.sqeakecleanblog.mediaplayer.model.structures.Playlist;
	
	import flash.events.*;
	
	[Event(name="change", type="com.sqeakecleanblog.mediaplayer.events.PlaylistNavEvent")]
	[Event(name="change", type="com.ghostmonk.events.PercentageEvent")]
	[Event(name="play", type="com.ghostmonk.events.video.VideoControlEvent")]
	[Event(name="pause", type="com.ghostmonk.events.video.VideoControlEvent")]
	
	public class MediaControls extends VideoControlPanel
	{
		private var _playList:InteractivePlaylist;
		private var _playBtn:ClickActionButton;
		private var _pauseBtn:ClickActionButton;
		private var _fullScreen:ClickActionButton;
		private var _next:ClickActionButton;
		private var _back:ClickActionButton;
		private var _time:TimeDisplay;
		private var _playHead:Scroller;
		private var _volumeControl:VolumeContol;
		private var _spectrumSwitches:Array;
		private var _panelAssets:PanelAssets;
		
		public function MediaControls()
		{
			_playList = new InteractivePlaylist(playlist);
			_playBtn = new ClickActionButton(playBtn, onPlay);
			_pauseBtn = new ClickActionButton(pauseBtn, onStop);
			_fullScreen = new ClickActionButton(fullScreen, onFullScreen);
			_next = new ClickActionButton(next, onNavigate);
			_back = new ClickActionButton(back, onNavigate);
			_time = new TimeDisplay(timeConsole);
			_playHead = new Scroller(playHeadTrack, onStop, onPlay);
			_volumeControl = new VolumeContol(volume);
			
			_spectrumSwitches = [new Switch(switch01, onSpectrumChoice), new Switch(switch02, onSpectrumChoice), new Switch(switch03, onSpectrumChoice)];
			_panelAssets = new PanelAssets(_playBtn, _pauseBtn, _back, _next, _fullScreen, _time, _playHead, _playList);
			
			_playHead.addEventListener(PercentageEvent.CHANGE, onScroll);
			_playList.addEventListener(PlaylistNavEvent.PLAYLIST_CHANGE, onPlayListNav); 
		}
		
		public function getControls():PanelAssets
		{
			return _panelAssets;
		}
		
		public function reset():void
		{
			_panelAssets.resetPositions();
			addChild(_playBtn.view);
			addChild(_pauseBtn.view);
			addChild(_next.view);
			addChild(_back.view);
			addChild(_fullScreen.view)
			addChild(_time.view);
			addChild(_playHead.view);
			addChild(_playList.view);
			
		}
		
		public function createPlayList(list:Playlist, startPos:int):void
		{
			_playList.createDisplay(list, startPos);
		}
		
		public function setView(node:int):void
		{
			_playList.setView(node);
		}
		
		public function timeChange(time:Number, loadPercent:Number, playPercent:Number):void
		{
			_time.setTime(time);
			_playHead.onLoad(loadPercent);
			_playHead.setScrubber(playPercent);
		}
		
		public function disable():void
		{
			_playHead.disable();
		}
		
		public function enable():void
		{
			_playHead.enable();
		}
		
		private function onScroll(e:PercentageEvent):void
		{
			dispatchEvent(e);
		}
		
		private function onNavigate(e:MouseEvent):void
		{
			var value:int = e.target == back ? -1 : 1;
			_playList.resetScrolling();
			dispatchEvent(new PlaylistNavEvent(PlaylistNavEvent.NEXT_PREVIOUS, value));
		}
		
		private function onPlay(e:MouseEvent):void
		{
			dispatchEvent(new VideoControlEvent(VideoControlEvent.PLAY));
		}
		
		private function onStop(e:MouseEvent):void
		{
			dispatchEvent(new VideoControlEvent(VideoControlEvent.PAUSE));
		}
		
		private function onFullScreen(e:MouseEvent):void
		{
			dispatchEvent(new FullScreenEvent(FullScreenEvent.FULL_SCREEN));
		}
		
		private function onPlayListNav(e:PlaylistNavEvent):void
		{
			dispatchEvent(e);
		}
		
		private function onSpectrumChoice(target:Switch):void
		{
			for each(var item:Switch in _spectrumSwitches)
			{
				item != target && item.setState(true);
			}
			
		 	dispatchEvent(new SwitchEvent(SwitchEvent.FLIP, _spectrumSwitches.indexOf(target)));
		}
		
	}
}