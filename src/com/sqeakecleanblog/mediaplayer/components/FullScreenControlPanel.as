package com.sqeakecleanblog.mediaplayer.components
{
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.ghostmonk.ui.TimeDisplay;
	import com.ghostmonk.ui.idecomposed.ClickActionButton;
	import com.ghostmonk.ui.idecomposed.Scroller;
	import com.sqeakecleanblog.mediaplayer.components.ui.InteractivePlaylist;
	import com.sqeakecleanblog.mediaplayer.model.structures.PanelAssets;
	
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	public class FullScreenControlPanel extends FullScreenPanel
	{
		private var _playBtn:ClickActionButton;
		private var _pauseBtn:ClickActionButton;
		private var _backBtn:ClickActionButton;
		private var _nextBtn:ClickActionButton;
		private var _fullScreen:ClickActionButton;
		private var _time:TimeDisplay;
		private var _playhead:Scroller;
		private var _playlist:InteractivePlaylist;
		
		private var _isMouseDown:Boolean;
		private var _timer:Timer;
		
		public function FullScreenControlPanel(ctrl:PanelAssets)
		{
			_playBtn = ctrl.playBtn;
			_pauseBtn = ctrl.pauseBtn;
			_backBtn = ctrl.backBtn;
			_nextBtn = ctrl.nextBtn;
			_fullScreen = ctrl.fullScreen;
			_time = ctrl.time;
			_playhead = ctrl.playhead;
			_playlist = ctrl.playlist;
			
			_isMouseDown = false;
			_timer = new Timer(5000,1);
			_timer.addEventListener(TimerEvent.TIMER_COMPLETE, hidePanel); 
			alpha = 0;
		}
		
		public function setFullScreen():void
		{
			_playBtn.view.x = resume.x;
			_playBtn.view.y = resume.y;
			
			_pauseBtn.view.x = pause.x;
			_pauseBtn.view.y = pause.y;
			
			_nextBtn.view.x = forward.x;
			_nextBtn.view.y = forward.y;
			
			_backBtn.view.x = back.x;
			_backBtn.view.y = back.y;
			
			_fullScreen.view.x = resize.x;
			_fullScreen.view.y = resize.y;
			
			_time.view.x = timer.x + 7;
			_time.view.y = timer.y + 3;
			
			_playhead.view.x = scroll.x;
			_playhead.view.y = scroll.y;
			
			_playlist.view.x = playlist.x;
			_playlist.view.y = playlist.y;
			
			addChild(_playBtn.view);
			addChild(_pauseBtn.view);
			addChild(_nextBtn.view);
			addChild(_backBtn.view);
			addChild(_fullScreen.view);
			addChild(_time.view);
			addChild(_playhead.view);
			addChild(_playlist.view);
			
			stage.addEventListener(MouseEvent.CLICK, onMouseClick);
			stage.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		}
		
		public function setNormalScreen():void
		{
			stage.removeEventListener(MouseEvent.CLICK, onMouseClick);
			stage.removeEventListener(MouseEvent.CLICK, onMouseClick);
			stage.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		}
		
		private function showPanel():void
		{
			Tweener.addTween(this, {alpha:1, y:stage.stageHeight - fullControlsBG.height - 60, time:0.4, transition:Equations.easeNone});
		}
		
		private function hidePanel(e:TimerEvent):void
		{
			if(!_isMouseDown)
			{
				Tweener.addTween(this, {alpha:0, y:stage.stageHeight, time:0.4, transition:Equations.easeNone});
			}
			else
			{
				onMouseClick(null);
			}
		}
		
		private function onMouseClick(e:MouseEvent):void
		{
			showPanel();
			_timer.stop();
			_timer.reset();
			_timer.start();
		}
		
		private function onMouseDown(e:MouseEvent):void
		{
			_isMouseDown = true;
		}
		
		private function onMouseUp(e:MouseEvent):void
		{
			_isMouseDown = false;
		}
		
	}
}