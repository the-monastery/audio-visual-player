package com.sqeakecleanblog.mediaplayer.components.media
{
	import com.ghostmonk.events.video.NetStreamEvent;
	import com.ghostmonk.media.audio.MusicPlayer;
	
	import flash.events.EventDispatcher;
	
	public class SqueakAudio extends EventDispatcher implements IMediaPlayer
	{
		private var _musicPlayer:MusicPlayer;
		
		public function SqueakAudio(musicPlayer:MusicPlayer)
		{
			_musicPlayer = musicPlayer;
			_musicPlayer.addEventListener(NetStreamEvent.STOP, onStopPlayBack);
		}

		public function pause():void
		{
			_musicPlayer.pause();
		}
		
		public function play():void
		{
			_musicPlayer.play();
		}
		
		public function shuttle(value:Number):void
		{
			_musicPlayer.seek(value);
		}
		
		public function get time():Number
		{
			return _musicPlayer.time;
		}
		
		public function get percentLoaded():Number
		{
			return _musicPlayer.percentLoaded;
		}
		
		public function seek(value:Number):void
		{
			_musicPlayer.seek(value);	
		}
		
		public function load(url:String):void
		{
			_musicPlayer.load(url);	
		}
		
		public function remove():void
		{
			_musicPlayer.close();
		}
		
		public function get duration():Number
		{
			return _musicPlayer.duration;
		}
		
		private function onStopPlayBack(e:NetStreamEvent):void
		{
			trace(e.type);
			dispatchEvent(new NetStreamEvent(e.type));
		}
		
	}
}