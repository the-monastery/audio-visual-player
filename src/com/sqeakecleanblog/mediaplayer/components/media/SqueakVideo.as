package com.sqeakecleanblog.mediaplayer.components.media
{
	import com.ghostmonk.events.video.NetStreamEvent;
	import com.ghostmonk.media.video.VideoScreen;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	public class SqueakVideo extends EventDispatcher implements IMediaPlayer
	{
		private var _videoScreen:VideoScreen;
		
		public function SqueakVideo(videoScreen:VideoScreen)
		{
			_videoScreen = videoScreen;
			_videoScreen.addEventListener(NetStreamEvent.STOP, onStopPlayBack);
		}

		public function pause():void
		{
			_videoScreen.pause();
		}
		
		public function play():void
		{
			_videoScreen.play();
		}
		
		public function shuttle(value:Number):void
		{
			_videoScreen.seek(value);
		}
		
		public function get time():Number
		{
			return _videoScreen.time;
		}
		
		public function get percentLoaded():Number
		{
			return _videoScreen.percentLoaded;
		}
		
		public function seek(value:Number):void
		{
			_videoScreen.seek(value);
		}
		
		public function load(url:String):void
		{
			_videoScreen.load(url);
		}
		
		public function remove():void
		{
			_videoScreen.close();
		}
		
		public function get duration():Number
		{
			return _videoScreen.duration;
		}
		
		private function onStopPlayBack(e:Event):void
		{
			trace(e.type);
			dispatchEvent(new NetStreamEvent(e.type));
		}
		
	}
}