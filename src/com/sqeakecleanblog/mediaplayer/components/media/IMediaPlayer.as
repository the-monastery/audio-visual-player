package com.sqeakecleanblog.mediaplayer.components.media
{
	public interface IMediaPlayer
	{
		function get time():Number;
		function get percentLoaded():Number;
		function pause():void;
		function play():void;
		function shuttle(value:Number):void;
		function seek(value:Number):void;
		function load(url:String):void;
		function remove():void;
		function get duration():Number;
	}
}