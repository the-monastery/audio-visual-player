package com.sqeakecleanblog.mediaplayer.components.ui
{
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.sqeakecleanblog.mediaplayer.events.PlaylistNavEvent;
	import com.sqeakecleanblog.mediaplayer.model.structures.Playlist;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;

	public class InteractivePlaylist extends EventDispatcher
	{
		private const _transitionTime:Number = 0.15;
		private var _listDisplayHeight:Number;
		private var _items:Array;
		private var _mainHolder:Sprite;
		private var _listholder:Sprite;
		private var _mask:Bitmap;
		private var _bg:Bitmap;
		
		private var _currentItem:int;
		
		private var _originY:Number;
		
		public function get view():Sprite
		{
			return _mainHolder;
		}
		
		public function InteractivePlaylist(holder:Sprite)
		{
			_mainHolder = holder;
			_originY = _mainHolder.y;
			_mainHolder.addEventListener(MouseEvent.ROLL_OVER, onRollover);
			_mainHolder.addEventListener(MouseEvent.ROLL_OUT, onRollout);
		}
		
		public function createDisplay(list:Playlist, currentItem:int):void
		{
			_items = new Array();
			_listholder = new Sprite();
			
			_currentItem = currentItem;
			
			for(var i:int = 0; i < list.length; i++)
			{
				var listItem:ListItem = new ListItem(list.getItem(i).title, i, onClick);
				listItem.y = listItem.height * i;
				_listholder.addChild(listItem);
				_items.push(listItem);
			}
			_listDisplayHeight = Math.min(_items[0].height * 20, (_items.length - 1)*_items[0].height); 
			createBG();
			_mask = new Bitmap(new BitmapData(_items[0].width,_items[0].height,false,0));
			_mainHolder.addChild(_mask);
			_mainHolder.mask = _mask;
			_mainHolder.addChild(_listholder);
			setView(_currentItem);
			dispatchEvent(new PlaylistNavEvent(PlaylistNavEvent.PLAYLIST_CHANGE, _currentItem));
		}
		
		public function setView(node:int):void
		{
			ListItem(_items[node]).isCurrent = true;
			ListItem(_items[node]).startScroll();
			_listholder.y = -_items[0].height * node;
		}
		
		public function resetScrolling():void
		{
			for each(var item:ListItem in _items)
			{
				item.isCurrent = false;
				item.endScroll();
			}
		}
		
		private function onClick(e:MouseEvent):void
		{
			_currentItem = ListItem(e.target).node;
			resetScrolling();
			dispatchEvent(new PlaylistNavEvent(PlaylistNavEvent.PLAYLIST_CHANGE, _currentItem));
			onRollout(null);
		}
		
		private function onRollover(e:MouseEvent):void
		{
			Tweener.addTween(_mask, {height:_listDisplayHeight+_items[0].height, width:_items[0].width + 15, y:-_listDisplayHeight, time:_transitionTime, transition:Equations.easeInQuad});
			Tweener.addTween(_bg, {y:-_listDisplayHeight, time:_transitionTime, transition:Equations.easeInQuad}); 
			Tweener.addTween(_listholder, {y:-_listDisplayHeight, time:_transitionTime, transition:Equations.easeInQuad}); 
		}
		
		private function onRollout(e:MouseEvent):void
		{
			Tweener.addTween(_mask, {height:_items[0].height, width:_items[0].width, y:0, time:_transitionTime, transition:Equations.easeOutQuad});	
			Tweener.addTween(_bg, {y:0, time:_transitionTime, transition:Equations.easeOutQuad});
			Tweener.addTween(_listholder, {y:-_currentItem * _items[0].height, time:_transitionTime, transition:Equations.easeOutQuad}); 
		}
		
		private function createBG():void
		{
			var item:ListItem = _items[_items.length-1];
			_bg = new Bitmap(new BitmapData(149, _listDisplayHeight + item.height,false,0x04002A));
			
			_mainHolder.addChildAt(_bg, 0);
		}
		
	}
}