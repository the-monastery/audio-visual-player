package com.sqeakecleanblog.mediaplayer.components
{
	import assets.VideoPlayer;
	
	import com.ghostmonk.media.SoundSpectrum;
	import com.ghostmonk.media.video.VideoScreen;

	public class TVScreen extends VideoPlayer
	{
		private var _soundSpectrum:SoundSpectrum;
		private var _videoScreen:VideoScreen;
		
		private var _baseX:Number;
		private var _baseY:Number;
		private var _baseWidth:Number;
		private var _baseHeight:Number;
		
		private var _thisX:Number;
		private var _thisY:Number;
		
		public function TVScreen(soundSpectrum:SoundSpectrum, videoScreen:VideoScreen)
		{
			_soundSpectrum = soundSpectrum;
			_videoScreen = videoScreen;
			
			_videoScreen.setSize(video.width, video.height);
			
			_baseX = video.x;
			_baseY = video.y;
			_baseHeight = video.height;
			_baseWidth = video.width;
			
			video.addChild(_soundSpectrum);
			video.addChild(_videoScreen);
		}
		
		public function fullScreen():void
		{
			chrome.visible = false;
			video.x = 0;
			video.y = 0;
			_thisX = x;
			_thisY = y;
			x = 0;
			y = 0;
			scaleX = stage.stageWidth/_baseWidth;
			scaleY = stage.stageHeight/_baseHeight;
		}
		
		public function normalScreen():void
		{
			chrome.visible = true;
			video.x = _baseX;
			video.y = _baseY;
			x = _thisX;
			y = _thisY;
			scaleX = 1;
			scaleY = 1;
		}
		
	}
}