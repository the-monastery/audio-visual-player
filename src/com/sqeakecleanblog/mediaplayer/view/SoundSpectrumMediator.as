package com.sqeakecleanblog.mediaplayer.view
{
	import com.ghostmonk.media.SoundSpectrum;
	import com.sqeakecleanblog.mediaplayer.events.SwitchEvent;
	
	import org.puremvc.as3.multicore.interfaces.INotification;
	import org.puremvc.as3.multicore.patterns.mediator.Mediator;

	public class SoundSpectrumMediator extends Mediator
	{
		public static const NAME:String = "SoundSpectrumMediator";
		
		public function get spectrum():SoundSpectrum 
		{
			return viewComponent as SoundSpectrum; 
		}
		
		public function SoundSpectrumMediator(viewComponent:SoundSpectrum)
		{
			super(NAME, viewComponent);
		}
		
		override public function listNotificationInterests():Array
		{
			return [MediaPlayerMediator.MEDIA_LOAD, SwitchEvent.FLIP];
		}
		
		override public function handleNotification(notification:INotification):void
		{
			
			switch(notification.getName())
			{
				case MediaPlayerMediator.MEDIA_LOAD: onMediaLoad(notification.getBody()); break;
				case SwitchEvent.FLIP: onSpectrumChoice(notification.getBody() as int); break;
			}
		}
		
		private function onMediaLoad(isMP3:Boolean):void
		{
			if(isMP3)
				spectrum.show();
			else
				spectrum.hide();
		}
		
		private function onSpectrumChoice(index:int):void
		{
			
		}
		
	}
}