package com.sqeakecleanblog.mediaplayer.view
{
	import com.sqeakecleanblog.mediaplayer.ApplicationFacade;
	import com.sqeakecleanblog.mediaplayer.components.*;
	import com.sqeakecleanblog.mediaplayer.events.*;
	import com.sqeakecleanblog.mediaplayer.model.PlaylistDataProxy;
	
	import flash.events.FullScreenEvent;
	
	import org.puremvc.as3.multicore.interfaces.INotification;
	import org.puremvc.as3.multicore.patterns.mediator.Mediator;

	public class MediaControlsMediator extends Mediator
	{
		public static const NAME:String = "MediaControlsMediator";
		
		private var _screen:TVScreen;
		
		public function get panel():MediaControls
		{
			return viewComponent as MediaControls;
		}
		
		public function MediaControlsMediator()
		{
			super(NAME, new MediaControls());
			panel.addEventListener(FullScreenEvent.FULL_SCREEN, onFullScreen);
			panel.addEventListener(SwitchEvent.FLIP, onSwitchFlip);
		}
		
		override public function listNotificationInterests():Array
		{
			return [
						ApplicationFacade.APP_INIT,
						ApplicationFacade.PLAYLIST_LOADED
					];
		}
		
		override public function handleNotification(notification:INotification):void
		{
			switch(notification.getName())
			{
				case ApplicationFacade.APP_INIT:
					_screen = facade.retrieveMediator(MediaPlayerMediator.NAME).getViewComponent() as TVScreen;
					break;
				case ApplicationFacade.PLAYLIST_LOADED:
					var playlistData:PlaylistDataProxy = facade.retrieveProxy(PlaylistDataProxy.NAME) as PlaylistDataProxy;
					panel.createPlayList(playlistData.playlist, playlistData.playlist.currentNode);
					break;
			}
		}
		
		private function onFullScreen(e:FullScreenEvent):void
		{
			sendNotification(ApplicationFacade.FULL_SCREEN);
		}
		
		private function onSwitchFlip(e:SwitchEvent):void
		{
			facade.sendNotification(e.type, e.index);
		}
		
	}
}