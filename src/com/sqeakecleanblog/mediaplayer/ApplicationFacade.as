package com.sqeakecleanblog.mediaplayer
{
	import com.sqeakecleanblog.mediaplayer.controller.StartupCommand;
	import com.sqeakecleanblog.mediaplayer.model.structures.StartupData;
	
	import org.puremvc.as3.multicore.patterns.facade.Facade;

	public class ApplicationFacade extends Facade
	{
		public static const STARTUP:String = "startup";
		public static const APP_INIT:String = "appInit";
		public static const PLAYLIST_LOADED:String = "playlistLoaded";
		public static const FULL_SCREEN:String = "fullScreen";
		
		public function ApplicationFacade(key:String)
		{
			super(key);
		}
		
		public static function getInstance(key:String):ApplicationFacade
		{
			if(instanceMap[key] == null) instanceMap[key] = new ApplicationFacade(key);
			return instanceMap[key] as ApplicationFacade;
		}
		
		override protected function initializeController():void
		{
			super.initializeController();
			registerCommand(STARTUP, StartupCommand);
		}
		
		public function startup(startupData:StartupData):void
		{
			sendNotification(STARTUP, startupData);
		}
		
	}
}