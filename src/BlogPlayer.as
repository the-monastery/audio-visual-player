package 
{
	import caurina.transitions.properties.DisplayShortcuts;
	
	import com.ghostmonk.ui.BaseContextMenu;
	import com.sqeakecleanblog.mediaplayer.ApplicationFacade;
	import com.sqeakecleanblog.mediaplayer.model.structures.StartupData;
	
	import flash.display.*;
	import flash.net.*;
	import flash.ui.*;

	[SWF(backgroundColor=0x04002A, width=600, height=580, frameRate=31)]

	public class BlogPlayer extends Sprite
	{
		public static const NAME:String = "BlogPlayer";
		
		public function BlogPlayer()
		{
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			DisplayShortcuts.init();
			var base:String = root.loaderInfo.parameters.playlistBase || "xmlDefault/";
			var listname:String = root.loaderInfo.parameters.listName || "default";
			var startNode:int = root.loaderInfo.parameters.itemID || -1;
			var startID:String = root.loaderInfo.parameters.startID || "";
			
			ApplicationFacade.getInstance(NAME).startup(new StartupData(stage, base + listname + ".xml", startNode, startID));
			//ApplicationFacade.getInstance(NAME).startup(new StartupData(stage, "xmlData/playlist.xml", startID));
			var contextMenu:BaseContextMenu = new BaseContextMenu(this);
			contextMenu.addLink("Programmed by Ghostmonk", "http://selectedwork.ghostmonk.com");
		}
		
	}
}